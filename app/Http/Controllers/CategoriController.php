<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Categori;

class CategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $categori = Categori::all();
        $categori = Categori::latest()->get();
        return view('categori.index', compact('categori'));
    }

    public function create()
    {
        return view('categori.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_kategori'=>'required|min:3',
        ]);

        Categori::create([
            'nama_kategori' => $request->nama_kategori,
            'slug' => \Str::slug($request->nama_kategori),
        ]);

        return redirect()->route('categori.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $categori = Categori::find($id);

        return view('categori.edit',compact('categori'));
    }

    public function update(Request $request, $id)
    {
        $categori = Categori::find($id);

        // $nama_kategori = $request->nama_kategori;
        // $slug = \Str::slug($request->nama_kategori);

        // var_dump($nama_kategori);
        // dd($slug);

        // $categori->update($request->all());

        $categori->update([
            'nama_kategori' => $request->nama_kategori,
            'slug' => \Str::slug($request->nama_kategori),
        ]);

        return redirect()->route('categori.index');
    }

    public function destroy($id)
    {
        
        $categori = Categori::find($id);

        if (!$categori){
            return redirect()->back();
        }

        $categori->delete();
        return redirect()->route('categori.index');
    }
}
