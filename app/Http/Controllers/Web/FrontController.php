<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Categori;
use App\Artikel;

class FrontController extends Controller
{
    public function index()
    {

        $categori = Categori::all();

        $artikel = Artikel::latest()->get()->random(2);

        $artikelall = Artikel::latest()->get();
        $artikelterkait = Artikel::latest()->limit(4)->get();

        return view('front',compact('categori','artikel','artikelall','artikelterkait'));
    }

    public function show(Artikel $artikel)
    {
        // var_dump('hahaha');
        // die;
        // $artikel_detail = $artikel;
        // dd($artikel);

        
        $artikelterkait = Artikel::latest()->get()->random(3);
        $categori = Categori::withCount('Artikel')->get();

       
    
        return view ('front.artikel_detail',compact('artikel','categori','artikelterkait'));
    }

    public function artikel_kategori(Categori $kategori)
    {
        $categori = Categori::all();

        $artikel = Artikel::latest()->get()->random(2);

        $artikelall = $kategori->Artikel()->get();
        $artikelterkait = Artikel::latest()->limit(4)->get();

        return view('front',compact('categori','artikel','artikelall','artikelterkait'));

        return $artikelall;
    }

    public function about()
    {
        $categori = Categori::all();

        return view ('front.about', compact('categori'));
    }

    public function contact()
    {
        $categori = Categori::all();

        return view ('front.contact', compact('categori'));
    }

}